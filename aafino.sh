#!/bin/bash
#
#exec 5>> /home/pi/nuvole/debug_output.txt
#BASH_XTRACEFD="5"
#PS4='$LINENO: '
#set -x
#source /usr/bin/fifino.sh
#si sposta nella cartella di lavoro
cd /home/pi/nuvole/
#crea cartella con nome data e ci entra dentro
mkdir -p "$(date +%F)" && cd "$_"
#effettua uno scatto di test per mettere a fuoco
/usr/local/bin/libcamera-still -n -o test.jpg --autofocus
#la camera registra foto dove -t da la durata in ms e --timelapse quanto passa tra un fotogramma e l'altro
#/usr/local/bin/libcamera-still -t 300000 -o %05d.jpg --timelapse 7000 --immediate 1 ## 5min
#/usr/local/bin/libcamera-still -t 600000 -o %05d.jpg --timelapse 7000 --immediate 1 ## 10min
/usr/local/bin/libcamera-still -t 1800000 -o %05d.jpg --timelapse 7000 --immediate 1 ## 30min
#/usr/local/bin/libcamera-still -t 3600000 -o %05d.jpg --timelapse 7000 --immediate 1 ## 1 ora
#/usr/local/bin/libcamera-still -t 7200000 -o %05d.jpg --timelapse 7000 --immediate 1 ## 2 ora
#/usr/local/bin/libcamera-still -t 10800000 -o %05d.jpg --timelapse 7000 --immediate 1 ## 3 ora
#/usr/local/bin/libcamera-still -t 14400000 -o %05d.jpg --timelapse 7000 --immediate 1 ## 4 ora
#/usr/local/bin/libcamera-still -t 18000000 -o %05d.jpg --timelapse 7000 --immediate 1 ## 5 ora
#/usr/local/bin/libcamera-still -t 21600000 -o %05d.jpg --timelapse 7000 --immediate 1 ## 6 ora
#/usr/local/bin/libcamera-still -t 25200000 -o %05d.jpg --timelapse 7000 --immediate 1 ## 7 ora
#/usr/local/bin/libcamera-still -t 28800000 -o %05d.jpg --timelapse 7000 --immediate 1 ## 8 ora
#/usr/local/bin/libcamera-still -t 32400000 -o %05d.jpg --timelapse 7000 --immediate 1 ## 9 ora
#/usr/local/bin/libcamera-still -t 36000000 -o %05d.jpg --timelapse 7000 --immediate 1 ## 10 ora
#/usr/local/bin/libcamera-still -t 39600000 -o %05d.jpg --timelapse 7000 --immediate 1 ## 11 ora
#/usr/local/bin/libcamera-still -t 43200000 -o %05d.jpg --timelapse 7000 --immediate 1 ## 12 ora
#/usr/local/bin/libcamera-still -t 46800000 -o %05d.jpg --timelapse 7000 --immediate 1 ## 13 ora
#/usr/local/bin/libcamera-still -t 50400000 -o %05d.jpg --timelapse 7000 --immediate 1 ## 14 ora
#/usr/local/bin/libcamera-still -t 54000000 -o %05d.jpg --timelapse 7000 --immediate 1 ## 15 ore
#/usr/local/bin/libcamera-still -t 57600000 -o %05d.jpg --timelapse 7000 --immediate 1 ## 16 ore
#/usr/local/bin/libcamera-still -t 61200000 -o %05d.jpg --timelapse 7000 --immediate 1 ## 17 ore
#/usr/local/bin/libcamera-still -t 64800000 -o %05d.jpg --timelapse 7000 --immediate 1 ## 18 ore
#/usr/local/bin/libcamera-still -t 68400000 -o %05d.jpg --timelapse 7000 --immediate 1 ## 19 ore
#/usr/local/bin/libcamera-still -t 72000000 -o %05d.jpg --timelapse 7000 --immediate 1 ## 20 ore
#/usr/local/bin/libcamera-still -t 75600000 -o %05d.jpg --timelapse 7000 --immediate 1 ## 21 ore
#/usr/local/bin/libcamera-still -t 79200000 -o %05d.jpg --timelapse 7000 --immediate 1 ## 22 ore
#/usr/local/bin/libcamera-still -t 82800000 -o %05d.jpg --timelapse 7000 --immediate 1 ## 23 ore
#/usr/local/bin/libcamera-still -t 86400000 -o %05d.jpg --timelapse 7000 --immediate 1 ## 24 ore
#/usr/local/bin/libcamera-still -t 43200000 -o %05d.jpg --timelapse 7000 --immediate 1
#/usr/local/bin/libcamera-still -t 43200000 -o %05d.jpg --timelapse 7000 --immediate 1
#/usr/local/bin/libcamera-still -t 43200000 -o %05d.jpg --timelapse 7000 --immediate 1
#/usr/local/bin/libcamera-still -t 43200000 -o %05d.jpg --timelapse 7000 --immediate 1
#/usr/local/bin/libcamera-still -t 43200000 -o %05d.jpg --timelapse 7000 --immediate 1
#quando ha finito la registrazione crea un video
mencoder "mf://*.jpg" -mf fps=25:type=jpg -ovc lavc -lavcopts vcodec=mpeg4:mbd=2:trell:vbitrate=7000 -vf scale=640:480 -oac copy -o movie$(date -I).avi
#Aspetta 60 sec e rimuove tutti i .jpg dalla cartella
sleep 60
rm *.jpg
exit
